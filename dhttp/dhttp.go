package dhttp

import (
	"bufio"
	"gitlab.com/dabick/go-util/log"
	"net/http"
	"os"
	"strconv"
	"strings"
)

const ONE_YEAR = 60 * 60 * 24 * 365

var (
	fileToETag map[string]string = make(map[string]string)
)

func init() {
	file, err := os.Open("cache.txt")
	if err != nil {
		log.Fatal(err)
	}
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		fileToValue := strings.SplitN(scanner.Text(), ",", 2)
		if len(fileToValue) != 2 {
			log.Println("Invalid Entry in cache.text", fileToValue)
			continue
		}
		fileToETag[fileToValue[0]] = fileToValue[1]
	}
	if err = scanner.Err(); err != nil {
		log.Println(err)
	}
}

func NotModified(w http.ResponseWriter, r *http.Request) bool {
	if eTag, ok := fileToETag[r.URL.Path]; ok {
		w.Header().Set("ETag", eTag)
		w.Header().Set("Cache-Control", "max-age="+strconv.Itoa(ONE_YEAR))
		if r.Header.Get("If-None-Match") == eTag {
			w.WriteHeader(304)
			return true
		}
	}
	return false
}
