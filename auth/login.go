package auth

import (
	"database/sql"
	"errors"
	"github.com/gorilla/securecookie"
	"github.com/gorilla/sessions"
	"gitlab.com/dabick/go-util/config"
	"gitlab.com/dabick/go-util/db"
	"gitlab.com/dabick/go-util/log"
	"golang.org/x/crypto/bcrypt"
	"net/http"
)

type Authorization struct {
	store *sessions.CookieStore
	cost  int
}

var auth Authorization

func init() {
	options := config.GetConfig()
	store := sessions.NewCookieStore(options.AuthenticationKey, options.EncryptionKey)
	store.Options = &sessions.Options{
		Domain:   "https://dbickler.com",
		Path:     "/",
		MaxAge:   43200, //12 hours
		Secure:   true,
		HttpOnly: true,
	}
	auth = Authorization{store, options.PasswordCost}
}

func generateKeysIfNecessary(options *Config) bool {
	changed := false
	if options.AuthenticationKey == nil || len(options.AuthenticationKey) == 0 {
		options.AuthenticationKey = securecookie.GenerateRandomKey(64)
		changed = true
	}
	if options.EncryptionKey == nil || len(options.EncryptionKey) == 0 {
		options.EncryptionKey = securecookie.GenerateRandomKey(32)
		changed = true
	}
	if options.PasswordCost < 10 {
		options.PasswordCost = 10
		changed = true
	}
	return changed
}

func Login(w http.ResponseWriter, r *http.Request) {
	username := r.FormValue("username")
	if err := testPassword(username, r.FormValue("password")); err != nil {
		handleError(w, 401, "Invalid username/password.", err.Error())
		return
	}
	authSession, _ := auth.store.Get(r, "auth")
	authSession.Values["username"] = username
	err := authSession.Save(r, w)
	if err != nil {
		serverError(w, err)
		return
	}
	//todo generate and save token and save to user db

	// usernameCookie := &http.Cookie{
	// 	Name:  "username",
	// 	Value: username,
	// 	// Path:  "/",
	// 	// Domain: options.Domain,
	// 	MaxAge:   int((12 * time.Hour).Seconds()), //12 hours todo: store in Config
	// 	Secure:   true,
	// 	HttpOnly: true,
	// }
	// http.SetCookie(w, usernameCookie)

	// w.Header().Add("Content-Type", "application/json")
	//w.Write([]byte("{\"token\":\:" + token + "\"}"))
}

func testPassword(username, password string) error {
	var dbPassword string
	if err := db.GetDb().QueryRow("SELECT password FROM users WHERE username = $1", username).Scan(&dbPassword); err != nil {
		return err
	}

	if err := bcrypt.CompareHashAndPassword([]byte(dbPassword), []byte(password)); err != nil {
		return errors.New(getPasswordError(err))
	}
	return nil
}

func getPasswordError(err error) string {
	switch error := err.(type) {
	case bcrypt.HashVersionTooNewError:
		return error.Error()
	case bcrypt.InvalidCostError:
		return error.Error()
	case bcrypt.InvalidHashPrefixError:
		return error.Error()
	default:
		return "Passwords didn't match."
	}
}

func serverError(w http.ResponseWriter, err error) {
	handleError(w, 500, "An error occured. We're sorry for the incovenience. If the error persists, contact technical support.", err.Error())
}

func handleError(w http.ResponseWriter, code int, userError string, logError string) {
	w.WriteHeader(code)
	w.Write([]byte(userError))
	if len(logError) > 0 {
		log.Println(logError)
	}
}

func Logout(w http.ResponseWriter, r *http.Request) {
	if !IsLoggedIn(r) {
		http.Redirect(w, r, "/recipes", http.StatusFound)
		return
	}

	authSession, _ := auth.store.Get(r, "auth")
	//todo should populate other options??
	authSession.Options = &sessions.Options{MaxAge: -1}
	authSession.Values["username"] = ""
	err := authSession.Save(r, w)
	if err != nil {
		serverError(w, err)
		return
	}

	// usernameCookie := &http.Cookie{
	// 	Name:  "username",
	// 	Value: "",
	// 	Path:  "/",
	// 	// Domain: options.Domain,
	// 	MaxAge:   -1,
	// 	Secure:   true,
	// 	HttpOnly: true,
	// }
	// http.SetCookie(w, usernameCookie)

	http.Redirect(w, r, "/recipes", http.StatusFound)
}

func Signup() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		password := r.FormValue("password")
		confirmPassword := r.FormValue("confirmPassword")
		username := r.FormValue("username")
		email := r.FormValue("email")

		if password != confirmPassword {
			validationErr := "Passwords don't match."
			handleError(w, 400, validationErr, validationErr)
			return
		}

		validationErr, dbError := verifyUnique(username, email)
		if len(validationErr) > 0 {
			handleError(w, 400, validationErr, validationErr)
			return
		} else if dbError != nil {
			serverError(w, dbError)
			return
		}

		encryptedPassword, err := bcrypt.GenerateFromPassword([]byte(password), auth.cost)
		if err != nil {
			serverError(w, err)
			return
		}

		_, err = db.GetDb().Exec("INSERT INTO users (username, email, password) VALUES ($1, $2, $3)", username, email, string(encryptedPassword))
		if err != nil {
			serverError(w, err)
			return
		}

		Login(w, r)
	})
}

func verifyUnique(username string, email string) (string, error) {
	var existingUser, existingEmail string
	err := db.GetDb().QueryRow("SELECT username, email FROM users WHERE username = $1 or email = $2", username, email).Scan(&existingUser, &existingEmail)
	switch {
	case err == sql.ErrNoRows:
		//Was unique
		return "", nil
	case err != nil:
		return "", err
	case len(existingUser) > 0:
		return "Username already exists", nil
	case len(existingEmail) > 0:
		return "Email already exists", nil
	}
	return "", nil
}

func SecureHandler(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if IsLoggedIn(r) {
			log.Debug("Logged in... serving http")
			h.ServeHTTP(w, r)
		} else {
			log.Debug("Not logged in... Returning 401.")
			w.WriteHeader(401)
		}
	})
}

func IsLoggedIn(r *http.Request) bool {
	authSession, _ := auth.store.Get(r, "auth")
	if username, ok := authSession.Values["username"].(string); ok {
		log.Debug(username)
		switch username {
		case "":
			return false
		default:
			return true
		}
	}
	return false
}
