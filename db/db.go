package db

import (
	"database/sql"
	_ "github.com/lib/pq"
	"gitlab.com/dabick/go-util/config"
	"gitlab.com/dabick/go-util/log"
)

const (
	user_property = "DbUser"
	name_property = "DbName"
	pass_property = "DbPass"
	ssl_property  = "DbSsl"
)

var (
	dbs = make(map[string]*sql.DB)
)

func InitDatabase(name string) {
	dataSource := "user=" + getProperty(name, user_property) +
		" dbname=" + getProperty(name, name_property) +
		" password=" + getProperty(name, pass_property) +
		" sslmode=" + getProperty(name, ssl_property)

	var db *sql.DB
	var err error
	if db, err = sql.Open("postgres", dataSource); err != nil {
		log.Fatal("Database failed to open " + err.Error())
	} else if err = db.Ping(); err != nil {
		log.Fatal("Database failed to open " + err.Error())
	}
	log.Println("Connected to database " + name)
	dbs[name] = db
}

func getProperty(name, propertyName string) string {
	prop := config.GetProperty(name, propertyName)
	if propString, ok := prop.(string); ok {
		return propString
	}
	panic("Invalid database property for name,propertyName " + name + "," + propertyName)
}

func GetDb(name string) *sql.DB {
	return dbs[name]
}

/*
todo for future ?
func Transact(db *sql.DB, txFunc func(*sql.Tx) error) (err error) {
    tx, err := db.Begin()
    if err != nil {
        return
    }
    defer func() {
        if p := recover(); p != nil {
            switch p := p.(type) {
            case error:
                err = p
            default:
                err = fmt.Errorf("%s", p)
            }
        }
        if err != nil {
            tx.Rollback()
            return
        }
        err = tx.Commit()
    }()
    return txFunc(tx)
}

This allows me to do this instead:

func (s Service) DoSomething() error {
    return Transact(s.db, func (tx *sql.Tx) error {
        if _, err := tx.Exec(...); err != nil {
            return err
        }
        if _, err := tx.Exec(...); err != nil {
            return err
        }
    })
}
*/
