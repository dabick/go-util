package config

import (
	"encoding/json"
	"io/ioutil"
)

/* Default Options that apply across apps should be stored in options.json and accessed through DEFAULT.
   Otherwise, each module needing configuration can provide a filename to load (.json will be appended) and access it through that.

   DEFAULT PROPERTIES:
*/

const (
	default_filename = "options"
)

var (
	options = make(map[string]map[string]interface{})
)

func init() {
	InitConfig(default_filename)
}

func InitConfig(category string) {
	if err := loadOptions(category); err != nil {
		panic(err)
	}
}

func GetProperty(category, propertyName string) interface{} {
	return options[category][propertyName]
}

func UpdateProperty(category, propertyName string, value interface{}) error {
	options[category][propertyName] = value
	return saveOptions(category)
}

func GetDefaultProperty(propertyName string) interface{} {
	return options[default_filename][propertyName]
}

func loadOptions(category string) error {
	file, err := ioutil.ReadFile(getFilename(category))
	if err != nil {
		return err
	}

	categoryOptions := make(map[string]interface{})
	if err = json.Unmarshal(file, &categoryOptions); err != nil {
		return err
	}
	options[category] = categoryOptions

	return nil
}

func getFilename(category string) string {
	return category + ".json"
}

func saveOptions(category string) error {
	updatedOptions, err := json.Marshal(options[category])
	if err != nil {
		return err
	}
	if err = ioutil.WriteFile(getFilename(category), updatedOptions, 0); err != nil {
		return err
	}
	return nil
}
