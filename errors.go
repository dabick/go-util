package goutil

type recipeError struct {
	code    int
	message string
	clean   string
}

func (err *recipeError) Error() string {
	return err.clean
}
