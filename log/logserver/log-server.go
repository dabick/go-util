package logserver

import (
	"github.com/gorilla/mux"
	"gitlab.com/dabick/go-util/log"
	"gitlab.com/dabick/go-util/log/setting"
	"net/http"
	"strconv"
)

func SetupLoggingServer(router *mux.Router) {
	router.HandleFunc("/log/level", getLevel).Methods("GET")
	router.HandleFunc("/log/level", setLevel).Methods("PUT")
}

func getLevel(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte(strconv.Itoa(int(setting.GetLevel()))))
}

func setLevel(w http.ResponseWriter, r *http.Request) {
	level := r.FormValue("level")
	if err := setting.UpdateLevel(level); err != nil {
		w.WriteHeader(500)
		log.Println(err)
		return
	}
}
