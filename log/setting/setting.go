package setting

import (
	"errors"
	"gitlab.com/dabick/go-util/config"
	"log"
)

const (
	FINEST = uint8(1)
	FINER  = uint8(2)
	FINE   = uint8(3)
	DEBUG  = uint8(4)
	INFO   = uint8(5)
	WARN   = uint8(6)
	ERROR  = uint8(7)
)
const (
	FINEST_NAME   = "FINEST"
	FINER_NAME    = "FINER"
	FINE_NAME     = "FINE"
	DEBUG_NAME    = "DEBUG"
	INFO_NAME     = "INFO"
	WARN_NAME     = "WARN"
	ERROR_NAME    = "ERROR"
	FILE_NAME     = "log"
	levelProperty = "level"
)

var currentLevel uint8 = INFO

func init() {
	config.InitConfig(FILE_NAME)
	level := config.GetProperty(FILE_NAME, levelProperty)
	if tempLevel, ok := level.(uint8); ok {
		currentLevel = tempLevel
	} else {
		currentLevel = INFO
	}

	if err := saveLevel(); err != nil {
		log.Fatal(err)
	}
}

func saveLevel() error {
	return config.UpdateProperty(FILE_NAME, levelProperty, currentLevel)
}

func GetLevel() uint8 {
	return currentLevel
}

func UpdateLevel(level string) error {
	switch level {
	case FINEST_NAME:
		currentLevel = FINEST
	case FINER_NAME:
		currentLevel = FINER
	case FINE_NAME:
		currentLevel = FINE
	case DEBUG_NAME:
		currentLevel = DEBUG
	case INFO_NAME:
		currentLevel = INFO
	case WARN_NAME:
		currentLevel = WARN
	case ERROR_NAME:
		currentLevel = ERROR
	default:
		return errors.New("Invalid value for level.")
	}
	return saveLevel()
}
