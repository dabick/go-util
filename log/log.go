package log

import (
	"fmt"
	"gitlab.com/dabick/go-util/log/setting"
	"log"
	"os"
)

var (
	logger *log.Logger
	debug  *log.Logger
)

func init() {
	logFile, err := os.Create(setting.FILE_NAME + ".log")
	if err != nil {
		log.Fatal(err.Error())
	}

	debugFile, err := os.Create(setting.FILE_NAME + ".debug")
	if err != nil {
		log.Fatal(err.Error())
	}

	logger = log.New(logFile, "", log.LstdFlags)
	debug = log.New(debugFile, "", log.LstdFlags|log.Lshortfile)
}

func Println(message ...interface{}) {
	if setting.GetLevel() <= setting.DEBUG {
		fmt.Println(message)
	}
	logger.Println(message)
}

func Printf(format string, v ...interface{}) {
	if setting.GetLevel() <= setting.DEBUG {
		fmt.Printf(format, v)
	}
	logger.Printf(format, v)
}

//Log to the log file and console, then kill the app
func Fatal(v ...interface{}) {
	fmt.Println("Fatal Error:")
	fmt.Println(v)
	logger.Fatal(v)
}

func Debug(message ...interface{}) {
	if setting.GetLevel() > setting.DEBUG {
		return
	}
	fmt.Println(message)
	debug.Println(message)
}
