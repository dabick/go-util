package derr

type WebError struct {
	Code    int
	Message string
	Clean   string
}

func (err *WebError) Error() string {
	return err.Clean
}
